# FE Test Intention #

Ovo je Intention test za Frontend Developere.

### Šta sadrži test? ###

* HTML / CSS
* JavaScript
* GIT, validacija

### Kako početi? ###

* Fork-uj projekat ukoliko znaš da radiš sa GIT-om
* Ukoliko ne znaš klikni levo na Downloads i download-uj ceo repository

### Kako dalje? ###

* Definiši name neki termin do kada možeš završiti test zadatak
* Ne moraš sve da završiš, ali ono što završiš treba biti dobro i čisto

### Pitanja? ###

* Javi se na posao@intention.rs ili Skype-om "sasasem"